//
//  EditViewController.swift
//  Journal
//
//  Created by Zachary Cole on 2/8/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    internal var info = NSDictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.redColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
