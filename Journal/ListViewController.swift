//
//  ListViewController.swift
//  Journal
//
//  Created by Zachary Cole on 2/8/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    internal let path = NSBundle.mainBundle().pathForResource("EntriesPropertyList", ofType: "plist")
    internal var array = NSArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        array = NSArray.init(contentsOfFile: path!)!
        
        let tableView:UITableView = UITableView.init(frame: self.view.bounds, style: UITableViewStyle.Plain)
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.backgroundColor = UIColor.blueColor()
        
        self.view.addSubview(tableView)
        
    }
    
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return array.count
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("cell")
        
        if ((cell == nil)) {
            cell = UITableViewCell.init(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        }

        let dictionary = array.objectAtIndex(indexPath.row)
        
        cell?.textLabel?.text = (dictionary.objectForKey("title") as! String)
        
        return cell!;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        //Use if using code
        //    SecondViewController* svc = [SecondViewController new];
        //    svc.info = [array objectAtIndex:indexPath.row];
        //    [self.navigationController pushViewController:svc animated:YES];
        
        let evc = EditViewController()
        evc.info = (array.objectAtIndex(indexPath.row) as! NSDictionary)
        self.navigationController?.pushViewController(evc, animated: true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
