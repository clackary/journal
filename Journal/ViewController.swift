//
//  ViewController.swift
//  Journal
//
//  Created by Zachary Cole on 2/5/16.
//  Copyright © 2016 Zachary Cole. All rights reserved.
//

import UIKit


class ViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let listVC:ListViewController = ListViewController()
        let editVC:EditViewController = EditViewController()
        
        var arr = [UIViewController]()
        arr.append(listVC)
        arr.append(editVC)
        
        self.setViewControllers(arr, animated: true)
        
        listVC.tabBarItem = UITabBarItem.init(title: "Entries", image: nil, tag: 0)
        editVC.tabBarItem = UITabBarItem.init(title: "New Entry", image: nil, tag: 1)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

